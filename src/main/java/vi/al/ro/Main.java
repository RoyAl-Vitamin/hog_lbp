package vi.al.ro;

import org.apache.commons.cli.*;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.io.File;
import java.util.Calendar;

public class Main {

    static private final Logger log = LogManager.getLogger(Main.class);

    private static String pathName = null;

    private enum Extension { PNG, JPEG, JPG }

    public static void main(String args[]) {

        Option option = new Option("f", "file", true, "file or directory path");
        option.setArgs(1);
        option.setOptionalArg(false);
        option.setArgName("file or directory path");

        Options basicOptions = new Options();
        basicOptions.addOption(option);

        CommandLineParser cmdLineBasicParser = new BasicParser();
        CommandLine cmdLine;

        try {
            cmdLine = cmdLineBasicParser.parse(basicOptions, args);
            if (cmdLine.hasOption("f")) {
                pathName = cmdLine.getOptionValue("f");
            } else {
                throw new UnrecognizedOptionException("Unrecognized arg", "f");
            }
        } catch (UnrecognizedOptionException e) {
            log.debug("u must input -f <u path>");
            help(basicOptions);
        } catch (ParseException e) {
            //e.printStackTrace();
            log.error(e.getMessage());
            help(basicOptions);
        }

        File file = new File(pathName);
        if (file.isDirectory()) {
            for (File iFile : file.listFiles()) {
                if (iFile.isFile() && allowableExtension(iFile)) {
                    calc(iFile);
                }
            }
        } else if (file.isFile() && allowableExtension(file)) {
            calc(file);
        }

    }

    private static void calc(File tempFile) {
        HOG hog = new HOG();
        LBP lbp = new LBP();

        long start = Calendar.getInstance().getTimeInMillis();
        hog.calc(tempFile);
        lbp.calc(tempFile);
        log.info("file == \"" + tempFile.getAbsolutePath() + "\" ends with "
                + (Calendar.getInstance().getTimeInMillis() - start) + "ms");
    }

    /**
     * Смотрит, не заканчивается ли полное имя файла на допустимое раширение
     * @return true, если файл удвлетворяет хотя бы одному расширению во множестве Extension
     */
    private static boolean allowableExtension(File tempFile) {
        boolean result = false;
        for (Extension ext : Extension.values()) {
            if (tempFile.getAbsolutePath().endsWith(ext.toString().toLowerCase())) {
                result = true;
            }
        }
        return result;
    }

    private static void help(Options options) {
        HelpFormatter formater = new HelpFormatter();
        formater.printHelp("Main", options);
        System.exit(0);
    }
}
