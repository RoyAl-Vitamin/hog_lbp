package vi.al.ro;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/* UNDONE LBP переписать эти костыли */
public class LBP {

    static private final Logger log = LogManager.getLogger(LBP.class);

    private File file;

    private int h, w;

    private int lbp[][];

    private final String directoryName = "LBP_output";

    public void calc(File inFile) {
        file = inFile;
        try {
            BufferedImage image = ImageIO.read(file);

            w = image.getWidth();
            h = image.getHeight();

            int lbp_bright[][] = new int[h][w]; // яркость

            lbp = new int[h][w]; // результат работы

            for (int i = 0; i < h; i++) { // строки
                for (int j = 0; j < w; j++) { // столбцы
                    int clr = image.getRGB(j, i);
                    int red = (clr & 0x00ff0000) >> 16;
                    int green = (clr & 0x0000ff00) >> 8;
                    int blue = clr & 0x000000ff;
                    lbp_bright[i][j] = (red + green + blue) / 3; // вычесляем среднее - серое
                }
            }

            String b_0, b_1, b_2, b_3, b_4, b_5, b_6, b_7; // Импровизированные биты
            b_0 = b_1 = b_2 = b_3 = b_4 = b_5 = b_6 = b_7 = "";

            for (int i = 0; i < h; i+=1) { // строки
                for (int j = 0; j < w; j+=1) { // столбцы

                    /* UNDONE Собрать if-ы в цикл */
                    try {
                        if (lbp_bright[i][j] > lbp_bright[i - 1][j]) { // UP
                            b_0 = "0";
                        } else {
                            b_0 = "1";
                        }
                    } catch (ArrayIndexOutOfBoundsException e){
                        b_0 = "0";
                    }

                    try {
                        if (lbp_bright[i][j] > lbp_bright[i - 1][j + 1]) { // UP - RIGHT
                            b_1 = "0";
                        } else {
                            b_1 = "1";
                        }
                    } catch (ArrayIndexOutOfBoundsException e){
                        b_1 = "0";
                    }

                    try {
                        if (lbp_bright[i][j] > lbp_bright[i][j + 1]) { // RIGHT
                            b_2 = "0";
                        } else {
                            b_2 = "1";
                        }
                    } catch (ArrayIndexOutOfBoundsException e){
                        b_2 = "0";
                    }

                    try {
                        if (lbp_bright[i][j] > lbp_bright[i + 1][j + 1]) { // DOWN - RIGHT
                            b_3 = "0";
                        } else {
                            b_3 = "1";
                        }
                    } catch (ArrayIndexOutOfBoundsException e){
                        b_3 = "0";
                    }

                    try {
                        if (lbp_bright[i][j] > lbp_bright[i + 1][j]) { // DOWN
                            b_4 = "0";
                        } else {
                            b_4 = "1";
                        }
                    } catch (ArrayIndexOutOfBoundsException e){
                        b_4 = "0";
                    }

                    try {
                        if (lbp_bright[i][j] > lbp_bright[i + 1][j - 1]) { // DOWN - LEFT
                            b_5 = "0";
                        } else {
                            b_5 = "1";
                        }
                    } catch (ArrayIndexOutOfBoundsException e){
                        b_5 = "0";
                    }

                    try {
                        if (lbp_bright[i][j] > lbp_bright[i][j - 1]) { // LEFT
                            b_6 = "0";
                        } else {
                            b_6 = "1";
                        }
                    } catch (ArrayIndexOutOfBoundsException e){
                        b_6 = "0";
                    }

                    try {
                        if (lbp_bright[i][j] > lbp_bright[i - 1][j - 1]) { // UP - LEFT
                            b_7 = "0";
                        } else {
                            b_7 = "1";
                        }
                    } catch (ArrayIndexOutOfBoundsException e){
                        b_7 = "0";
                    }

                    lbp[i][j] = Integer.valueOf(b_7 + b_6 + b_5 + b_4 + b_3 + b_2 + b_1 + b_0,2);
                }
            }

            prepareToSave();

        } catch (IOException e) {
            e.printStackTrace();
            log.error(e.getMessage());
        }
    }

    private void prepareToSave() throws IOException {

        BufferedImage outputImg = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);

        for (int i = 0; i < h; i++) {
            for (int j = 0; j < w; j++) {
                Color color = null;
                try {
                    color = new Color(
                            lbp[i][j],
                            lbp[i][j],
                            lbp[i][j]);
                } catch (java.lang.IllegalArgumentException e) {
                    log.error(" lbp[i][j] == " + lbp[i][j]);
                }
                outputImg.setRGB(j, i, color.getRGB());
            }
        }

        saveResult(outputImg);
    }

    private void saveResult(BufferedImage image) throws IOException {

        File folder = new File(file.getParentFile(), directoryName);

        if (!folder.exists()) {
            folder.mkdir();
        }

        // UNDONE подправить расширение на *.png
        ImageIO.write(image, "PNG", new File(folder, "LBP_" + file.getName()));
    }
}
