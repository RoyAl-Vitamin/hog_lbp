package vi.al.ro;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/* выделяет контуры на изображении */
public class HOG {

    static private final Logger log = LogManager.getLogger(HOG.class);

    private final int numberOfSectors = 9;

    private class Point2D {
        int x, y;

        public Point2D() {
            x = y = 0;
        }

        public Point2D(int x1, int y1) {
            x = x1;
            y = y1;
        }
    }

    private Point2D[][] grad; // массив градиентов

    private int[] hog; // HOG гистограмма (распределение направлений градиентов)

    private int h, w;

    private File file;

    private final String directoryName = "HOG_output";

    public void calc(File inFile) {
        file = inFile;

        grad = null;
        hog = null;
        h = w = 0;

        try {
            BufferedImage image = ImageIO.read(file);

            h = image.getHeight();
            w = image.getWidth();
            grad = new Point2D[h][w];
            for (int i = 0; i < h; i++) {
                for (int j = 0; j < w; j++) {
                    grad[i][j] = new Point2D();
                    int clr_u = 0;
                    if (i - 1 >= 0) { // то что на границе должно быть чёрным
                        clr_u = image.getRGB(j, i - 1);
                    }
                    int red_u = (clr_u & 0x00ff0000) >> 16;
                    int green_u = (clr_u & 0x0000ff00) >> 8;
                    int blue_u = clr_u & 0x000000ff;
                    int brightness_up = red_u + green_u + blue_u;

                    int clr_d = 0;
                    if (i + 1 < h) {
                        clr_d = image.getRGB(j, i + 1);
                    }
                    int red_d = (clr_d & 0x00ff0000) >> 16;
                    int green_d = (clr_d & 0x0000ff00) >> 8;
                    int blue_d = clr_d & 0x000000ff;
                    int brightness_down = red_d + green_d + blue_d;

                    grad[i][j].x = brightness_down - brightness_up;

                    int clr_l = 0;
                    if (j - 1 >= 0) {
                        clr_l = image.getRGB(j - 1, i);
                    }
                    int red_l = (clr_l & 0x00ff0000) >> 16;
                    int green_l = (clr_l & 0x0000ff00) >> 8;
                    int blue_l = clr_l & 0x000000ff;
                    int brightness_left = red_l + green_l + blue_l;

                    int clr_r = 0;
                    if (j + 1 < w) {
                        clr_r = image.getRGB(j + 1, i);
                    }
                    int red_r = (clr_r & 0x00ff0000) >> 16;
                    int green_r = (clr_r & 0x0000ff00) >> 8;
                    int blue_r = clr_r & 0x000000ff;
                    int brightness_right = red_r + green_r + blue_r;

                    grad[i][j].y = brightness_right - brightness_left;
                }
            }

            whatISee();

            createHistogram();

            addHog();

        } catch (IOException e) {
            e.printStackTrace();
            log.error(e.getMessage());
        }
    }

    /**
     * Создаёт гистограмму ориентированных градиентов
     * <p>
     * 20 градусов на сектор сделал так по статье:
     * http://mccormickml.com/2013/05/09/hog-person-detector-tutorial/
     */
    // ERROR: 3 and 5 class is 0!!!
    private void createHistogram() {
        hog = new int[numberOfSectors];

        for (int i = 0; i < h; i++) {
            for (int j = 0; j < w; j++) {
                if (grad[i][j].x != 0) {
                    float alpha = (float) Math.atan(grad[i][j].y / grad[i][j].x);
                    //log.debug("alpha == " + alpha);

                    /* Распределяем на классы по 20 градусов */
                    for (int k = 0; k < numberOfSectors; k++) {
                        if (alpha < -Math.PI / 2 + (k + 1) * Math.PI / 9) {
                            hog[k]++;
                            k = numberOfSectors;
                        }
                    }
                } else {
                    if (grad[i][j].y > 0) {
                        hog[numberOfSectors - 1]++;
                    } else {
                        hog[0]++;
                    }
                }
            }
        }

        for (int k = 0; k < numberOfSectors; k++) {
            log.info("k = " + k + " : " + hog[k]);
        }
    }

    /**
     * Выводит изображение по величине градиента, чем длинее вектор градиента, тем более яркий пиксель выводится
     */
    private void whatISee() throws IOException {
        BufferedImage outputImg = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
        double max = 0;
        for (int i = 0; i < h; i++) {
            for (int j = 0; j < w; j++) {
                double module = Math.sqrt(Math.pow(grad[i][j].x, 2) + Math.pow(grad[i][j].y, 2));
                if (max < module) {
                    max = module;
                }
            }
        }
        for (int i = 0; i < h; i++) {
            for (int j = 0; j < w; j++) {
                int average = (int) (Math.sqrt(Math.pow(grad[i][j].x, 2) + Math.pow(grad[i][j].y, 2)) / max * 255);
                outputImg.setRGB(j, i, new Color(average, average,average).getRGB());
            }
        }
        File folder = new File(file.getParentFile(), directoryName);
        if (!folder.exists()) {
            folder.mkdir();
        }
        // UNDONE подправить расширение на *.png
        ImageIO.write(outputImg, "PNG", new File(folder, "HOG_" + file.getName()));
    }

    /**
     * Дополняет исходное изображение данными из HOG descriptor'a
     */
    private void addHog() throws IOException {
        BufferedImage outputImg = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
        BufferedImage inputImg = ImageIO.read(file);
        double max = 0;
        for (int i = 0; i < h; i++) {
            for (int j = 0; j < w; j++) {
                double module = Math.sqrt(Math.pow(grad[i][j].x, 2) + Math.pow(grad[i][j].y, 2));
                if (max < module) {
                    max = module;
                }
            }
        }
        for (int i = 0; i < h; i++) {
            for (int j = 0; j < w; j++) {
                double module = Math.sqrt(Math.pow(grad[i][j].x, 2) + Math.pow(grad[i][j].y, 2));
                int clr = inputImg.getRGB(j, i);
                int red = (clr & 0x00ff0000) >> 16;
                int green = (clr & 0x0000ff00) >> 8;
                int blue = clr & 0x000000ff;
                Color color = new Color(
                        255 - (int) (red * module / max),
                        255 - (int) (green * module / max),
                        255 - (int) (blue * module / max));
                outputImg.setRGB(j, i, color.getRGB());
            }
        }
        File folder = new File(file.getParentFile(), directoryName);
        if (!folder.exists()) {
            folder.mkdir();
        }
        // UNDONE подправить расширение на *.png
        ImageIO.write(outputImg, "PNG", new File(folder, "HOG_M_" + file.getName()));
    }
}
