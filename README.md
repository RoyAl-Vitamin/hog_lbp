## Реализация LBP и HOG детектора на Java  
Implementation HOG is based on articles:  
+ [HOG Person Detector Tutorial](http://mccormickml.com/2013/05/09/hog-person-detector-tutorial/)  
+ [Обучение машины — забавная штука: современное распознавание лиц с глубинным обучением](https://habrahabr.ru/post/306568/)  
+ [Алгоритм быстрого построения дескрипторов изображения, основанных на технике гистограмм ориентированных градиентов *SEE* **Гистограмма ориентированных градиентов**](https://mipt.ru/upload/993/84-91-arphj8g0g1k.pdf)  
+ [Histogram of Oriented Gradients](https://www.learnopencv.com/histogram-of-oriented-gradients/)  
-----------------------
Implementation LBP is based on articles:  
+ [Multi-Scale Feature Learning for Material Recognition *SEE* **2.1.1 Local Binary Pattern**](https://wenbinli.github.io/assets/pdf/wenbinli_msc_thesis.pdf)  
+ [Обзор дескрипторов изображения Local Binary Patterns (LBP) и их вариаций](https://habrahabr.ru/post/280888/)  
-----------------------
### For BUILD:  
Using `mvn package` for build app  

### For RUN:  
In CMD input `cd target`  
Use whit FILE:  
Input `java -jar HOG_LBP-1.0-SNAPSHOT.jar -f D:/Users/%user_name%/Desktop/picture_name.%jpg || png%`  
Or use whit DIRECTORY:  
Input `java -jar HOG_LBP-1.0-SNAPSHOT.jar -f D:/Users/%user_name%/Desktop/picture_folder`  